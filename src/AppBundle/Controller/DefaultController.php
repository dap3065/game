<?php

namespace AppBundle\Controller;

use AppBundle\Service\GamePlayService;
use AppBundle\Service\GameStatisticsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/played", name="played")
     */
    public function playedAction(Request $request)
    {
        /** @var GamePlayService $gamePlayService */
        $gamePlayService = $this->get('game_play');

        $game = $gamePlayService->playGame($request->getClientIp(), $request->request->get('play'));

        return $this->render('default/played.html.twig', [
            'game' => $game,
        ]);
    }

    /**
     * @Route("/statistics", name="statistics")
     */
    public function statisticsAction(Request $request)
    {
        /** @var GameStatisticsService $gameStatisticsService */
        $gameStatisticsService = $this->get('game_statistics');

        $statistics = $gameStatisticsService->getStatistics($request->getClientIp());
        return $this->render('default/statistics.html.twig', [
            'statistics' => $statistics,
        ]);
    }

}
