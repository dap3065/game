<?php

// src/AppBundle/Entity/Game.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="games")
 */
class Game
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="user", length=255)
     */
    private $user;

    /**
     * @ORM\Column(type="string", name="user_play", length=255)
     */
    private $userPlay;

    /**
     * @ORM\Column(type="string", name="computer_play", length=255)
     */
    private $computerPlay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $win;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUserPlay()
    {
        return $this->userPlay;
    }

    /**
     * @param mixed $userPlay
     */
    public function setUserPlay($userPlay)
    {
        $this->userPlay = $userPlay;
    }

    /**
     * @return mixed
     */
    public function getComputerPlay()
    {
        return $this->computerPlay;
    }

    /**
     * @param mixed $computerPlay
     */
    public function setComputerPlay($computerPlay)
    {
        $this->computerPlay = $computerPlay;
    }

    /**
     * @return boolean
     */
    public function isWin()
    {
        return $this->win;
    }

    /**
     * @param mixed $win
     */
    public function setWin($win)
    {
        $this->win = $win;
    }

}