<?php

// src/AppBundle/Entity/Game.php
namespace AppBundle\Service;

use AppBundle\Entity\Game;
use Doctrine\ORM\EntityManager;

class GamePlayService
{
    const ROCK = 'rock';
    const PAPER = 'paper';
    const SCISSORS = 'scissors';
    const SPOCK = 'spock';
    const LIZARD = 'lizard';

    /**
     * @var array
     */
    private $playChoices = [
        GamePlayService::ROCK,
        GamePlayService::PAPER,
        GamePlayService::SCISSORS,
        GamePlayService::SPOCK,
        GamePlayService::LIZARD
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * GamePlayService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @return array
     */
    public function getPlayChoices()
    {
        return $this->playChoices;
    }

    /**
     * @param array $playChoices
     */
    public function setPlayChoices($playChoices)
    {
        $this->playChoices = $playChoices;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $userIp
     * @param $userPlay
     * @return Game
     */
    public function playGame($userIp, $userPlay) {
        $win = false;
        $computerPlay = $this->getRandomPlay();
        // Ties considered a loss
        switch ($userPlay) {
            // Paper beats none except rock
            case GamePlayService::PAPER:
                switch ($computerPlay) {
                    case GamePlayService::PAPER:
                        $win = false;
                        break;
                    case GamePlayService::ROCK:
                        $win = true;
                        break;
                    case GamePlayService::SCISSORS:
                        $win = false;
                        break;
                    case GamePlayService::SPOCK:
                        $win = false;
                        break;
                    case GamePlayService::LIZARD:
                        $win = false;
                        break;
                }
                break;
                // Rock beats all but paper
            case GamePlayService::ROCK:
                switch ($computerPlay) {
                    case GamePlayService::PAPER:
                        $win = false;
                        break;
                    case GamePlayService::ROCK:
                        $win = false;
                        break;
                    case GamePlayService::SCISSORS:
                        $win = true;
                        break;
                    case GamePlayService::SPOCK:
                        $win = true;
                        break;
                    case GamePlayService::LIZARD:
                        $win = true;
                        break;
                }
                break;
                // Scissors beats all but rock
            case GamePlayService::SCISSORS:
                switch ($computerPlay) {
                    case GamePlayService::PAPER:
                        $win = true;
                        break;
                    case GamePlayService::ROCK:
                        $win = false;
                        break;
                    case GamePlayService::SCISSORS:
                        $win = false;
                        break;
                    case GamePlayService::SPOCK:
                        $win = true;
                        break;
                    case GamePlayService::LIZARD:
                        $win = true;
                        break;
                }
                break;
                // Spock can beat paper and a lizard
            case GamePlayService::SPOCK:
                switch ($computerPlay) {
                    case GamePlayService::PAPER:
                        $win = true;
                        break;
                    case GamePlayService::ROCK:
                        $win = false;
                        break;
                    case GamePlayService::SCISSORS:
                        $win = false;
                        break;
                    case GamePlayService::SPOCK:
                        $win = false;
                        break;
                    case GamePlayService::LIZARD:
                        $win = true;
                        break;
                }
                break;
                // Lizard can beat paper
            case GamePlayService::LIZARD:
                switch ($computerPlay) {
                    case GamePlayService::PAPER:
                        $win = true;
                        break;
                    case GamePlayService::ROCK:
                        $win = false;
                        break;
                    case GamePlayService::SCISSORS:
                        $win = false;
                        break;
                    case GamePlayService::SPOCK:
                        $win = false;
                        break;
                    case GamePlayService::LIZARD:
                        $win = false;
                        break;
                }
                break;
        }
        $game = new Game();
        $game->setUser($userIp);
        $game->setUserPlay($userPlay);
        $game->setComputerPlay($computerPlay);
        $game->setWin($win);
        $this->entityManager->persist($game);
        $this->entityManager->flush();
        return $game;
    }

    /**
     * @return string
     */
    public function getRandomPlay() {
        return array_rand(array_flip($this->getPlayChoices()));
    }
}